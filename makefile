#https://www.youtube.com/watch?v=aw9wHbFTnAQ 

# makefile version 1.0.10.23.2018

CFLAGS=-std=c++17 -g -Wall
CC=g++

all:	
	@printf "\033[36mCompiling \"sssp\"...\n\033[0m"
	@printf "\nRun by typing \033[34m 'make run'\n\n\033[0m"
	$(CC) $(CFLAGS) sssp.cpp -o sssp.out

# ...................................................................
run:
	@./sssp.out

# ...................................................................

# remove temp files

clean:
	rm -f *.out *.o 
